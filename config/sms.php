<?php

return [
    'username' => env('SMSAPI_USERNAME', ''),
    'password' => env('SMSAPI_PASSWORD', '')
];