<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => ['admin'], 'namespace' => 'Admin'], function () {
    CRUD::resource('page', 'PageCrudController');
    CRUD::resource('user', 'UserCrudController');

    Route::get('sendEmail/{user}', 'EmailController@index');
    Route::post('sendEmail', 'EmailController@send');

    Route::get('sendSms', 'SmsController@index');
    Route::post('sendSms', 'SmsController@send');
});
