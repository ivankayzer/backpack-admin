<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\MessageUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function index(User $user)
    {
        return view('emails.index')->with('user', $user);
    }

    public function send(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'message' => 'required'
        ]);

        Mail::to($request->email)->send(new MessageUser($request->all()));

        \Alert::success('Your message was sent successfully')->flash();

        return redirect()->route('crud.user.index');
    }
}
