<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\SmsService;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    private $sms;

    public function __construct(SmsService $sms)
    {
        $this->sms = $sms;
    }

    public function index()
    {
        return view('sms.index');
    }

    public function send(Request $request)
    {
        $this->validate($request, [
            'phone' => "required",
            'message' => "required"
        ]);

        $this->sms->to($request->phone)->send($request->message);

        \Alert::success('Your sms was sent successfully')->flash();

        return redirect()->route('backpack');
    }
}
