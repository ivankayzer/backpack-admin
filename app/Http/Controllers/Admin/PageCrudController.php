<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PageRequest as StoreRequest;
use App\Http\Requests\PageRequest as UpdateRequest;

class PageCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Page');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/page');
        $this->crud->setEntityNameStrings('page', 'pages');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

         $this->crud->addField([
             'label' => "Author",
             'type' => 'select',
             'name' => 'author',
             'entity' => 'author',
             'attribute' => 'name',
             'model' => "App\Models\User"
         ], 'update/create/both');

         $this->crud->addField([
             'label' => "Image",
             'name' => "image",
             'type' => 'image',
             'upload' => true,
             'crop' => true, // set to true to allow cropping, false to disable
             'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
         ]);

        // ------ CRUD COLUMNS
         $this->crud->addColumn([
             'name' => "author",
             'label' => "Author",
             'type' => "model_function",
             'function_name' => 'getAuthorName',
         ]);

        $this->crud->addColumn([
            'name' => "image",
            'label' => "Image",
            'type' => "model_function",
            'function_name' => 'getImagePreview',
        ]);

    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
