<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 04/01/2018
 * Time: 12:34
 */
namespace App\Services;

use SMSApi\Client;
use SMSApi\Api\SmsFactory;
use SMSApi\Exception\ClientException;
use SMSApi\Exception\SmsapiException;

class SmsService
{
    private $api;
    private $to;

    public function __construct()
    {
        $client = new Client(config('sms.username'));
        try {
            $client->setPasswordRaw(config('sms.password'));
        } catch (ClientException $e) {
            \Log::alert('SMS API user not found.');
        }

        $this->api = new SmsFactory;
        $this->api->setClient($client);
    }

    public function to($number)
    {
        $this->to = $number;
        return $this;
    }

    public function send($message)
    {
        try {
            $actionSend = $this->api->actionSend();

            $actionSend->setTo($this->to);
            $actionSend->setText($message);
            $actionSend->setSender('Info');

            $actionSend->execute();
        } catch (SmsapiException $exception) {
            \Log::alert($exception->getMessage());
        }
    }
}